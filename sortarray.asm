include 'emu8086.inc'
.MODEL SMALL
.DATA
    ARRAY DB 19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0
.CODE
.STARTUP
    MOV AX,DATA
    MOV DS,AX
    MOV CX,19 ; for n-1
    L1:
    MOV BX,CX
    MOV SI,0
    L2:
    MOV AL,ARRAY[SI]
    MOV DL, ARRAY[SI+1]
    CMP DL,AL
    JNC L3
    MOV ARRAY[SI],DL
    MOV ARRAY[SI+1],AL
    L3:
    INC SI
    DEC BX
    JNZ L2
    LOOP L1
    
    MOV BX,0
    MOV CX,20
    
Printarray:
    MOV AL,ARRAY[BX]
    CBW
    CALL print_num
    INC BX
    LOOP Printarray
    
DEFINE_PRINT_NUM
DEFINE_PRINT_NUM_UNS

.EXIT
END
